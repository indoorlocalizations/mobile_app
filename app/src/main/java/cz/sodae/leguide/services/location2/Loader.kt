package cz.sodae.leguide.services.location2

import android.content.Context
import com.cube.geojson.FeatureCollection
import com.cube.geojson.GeoJsonObject
import com.cube.geojson.LngLatAlt
import com.cube.geojson.gson.GeoJsonObjectAdapter
import com.cube.geojson.gson.LngLatAltAdapter
import com.google.gson.GsonBuilder
import cz.sodae.leguide.geoprojection.Point
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.elements.Element
import cz.sodae.leguide.geoprojection.graph.OverviewGraph
import cz.sodae.leguide.geoprojection.graph2.GraphBuilder
import cz.sodae.leguide.geoprojection.graph2.GraphStructure
import cz.sodae.leguide.geoprojection.graph2.PathFinder
import cz.sodae.leguide.geoprojection.loader.FeatureCenterFinder
import cz.sodae.leguide.geoprojection.loader.Loader
import cz.sodae.leguide.geoprojection.loader.geojson.CartesianPositionConverter
import cz.sodae.leguide.geoprojection.loader.geojson.ElementsConverter
import cz.sodae.leguide.geoprojection.loader.geojson.GeoJSONToGeometryConverter
import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates
import cz.sodae.leguide.utils.HashUtils
import java.io.File
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable

/**
 * Caching graph for path finding, it improves map loading at second time
 */
class CachedLoader(val context: Context, val geojson: String) {

    /**
     * Hash of [geojson]
     */
    private val hash = HashUtils.sha1(geojson)

    /**
     * File for cached file
     */
    private val file = File(context.cacheDir, hash)

    /**
     * Cartesian converter
     */
    val cartesian: CartesianPositionConverter

    /**
     * Elements of building
     */
    val elements: List<Element>

    /**
     * Building instance
     */
    val building: Building

    /**
     * Path finding
     */
    val pathFinder: PathFinder

    init {
        val struct = loadCache()

        val featureCollection = loadFeatureCollection()

        val center = struct?.center ?: FeatureCenterFinder().find(featureCollection)

        cartesian = CartesianPositionConverter(center)
        elements = loadElements(featureCollection)
        building = Building(elements)

        val graphStructure = struct?.graphStructure
                ?: GraphBuilder(building, OverviewGraph(building)).exportToGraphStructure()

        pathFinder = PathFinder(building, graphStructure)

        if (struct == null) {
            storeCache(Struct(
                    center,
                    graphStructure
            ))
        }
    }

    /**
     * Loads cached file with center and path graph or null if not exists
     */
    private fun loadCache(): Struct? {
        if (file.exists()) {
            return file.inputStream().use {
                ObjectInputStream(it).use {
                    Struct(
                            WSG84Coordinates(
                                    it.readDouble(),
                                    it.readDouble(),
                                    0.0
                            ),
                            it.readObject() as GraphStructure
                    )
                }
            }

        }
        return null
    }

    /**
     * Stores center and path graph
     */
    private fun storeCache(struct: Struct) {
        file.outputStream().use {
            file.createNewFile()
            ObjectOutputStream(it).use {
                it.writeDouble(struct.center.longitude)
                it.writeDouble(struct.center.latitude)
                it.writeObject(struct.graphStructure as Serializable)
            }
        }
    }

    /**
     * Loading features from json string
     */
    private fun loadFeatureCollection(): FeatureCollection {
        val builder = GsonBuilder()
        builder.registerTypeAdapter(GeoJsonObject::class.java, GeoJsonObjectAdapter())
        builder.registerTypeAdapter(LngLatAlt::class.java, LngLatAltAdapter())
        val gson = builder.create()

        return gson.fromJson<FeatureCollection>(geojson, FeatureCollection::class.java)
    }

    /**
     * Loads elements from features
     */
    private fun loadElements(featureCollection: FeatureCollection): List<Element> {
        var identifierCounter = 0
        val geoConverter = GeoJSONToGeometryConverter(cartesian)
        val el = ElementsConverter(geoConverter)
        return el.convert(featureCollection).map {
            it.identifier = "element-" + (identifierCounter++).toString().padStart(6, '0')
            it
        }
    }

    /**
     * Structure for saving cache
     */
    private data class Struct (
            val center: WSG84Coordinates,
            val graphStructure: GraphStructure
    )

}
