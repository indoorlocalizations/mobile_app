package cz.sodae.leguide.services.location2

import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates

/**
 * Represents a result from localisation
 */
class LevelLocation(
        val coordinates: WSG84Coordinates,
        val level: Double,
        val accuracy: Double = Double.NaN,
        val compass: Double = Double.NaN
)
