package cz.sodae.leguide.services.location2

import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Binder
import android.os.IBinder
import com.github.pwittchen.reactivesensors.library.ReactiveSensorFilter
import com.github.pwittchen.reactivesensors.library.ReactiveSensors
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import cz.sodae.leguide.geoprojection.building.Building
import cz.sodae.leguide.geoprojection.graph2.LevelPoint
import cz.sodae.leguide.geoprojection.model.WalkingAccelerometer
import cz.sodae.leguide.geoprojection.model.WalkingDetection
import cz.sodae.leguide.geoprojection.process.CurrentPosition
import cz.sodae.leguide.geoprojection.process.ReactiveProcess
import cz.sodae.leguide.geoprojection.process.Signal
import cz.sodae.leguide.geoprojection.utils.convertToCircleAngle
import cz.sodae.leguide.map.MapDataManifest
import cz.sodae.leguide.services.location2.sensorfusion.ImprovedOrientationSensor1Provider
import cz.sodae.leguide.utils.LogTag
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.TimeUnit

/**
 * Localisation service
 */
class LocationService : Service() {

    private val log = LogTag("LocationService")

    private lateinit var rxBluetooth: RxBleClient

    /**
     * Events from sensors
     */
    private lateinit var bleScanningRx: Flowable<Signal>
    private lateinit var compassRx: Flowable<Double>
    private lateinit var walkingRx: Flowable<Boolean>
    private lateinit var orientationSensor: ImprovedOrientationSensor1Provider

    /**
     * Events from loading and localisation
     */
    private lateinit var locationRx: Flowable<CurrentPosition>
    private lateinit var mapRx: Flowable<Pair<ChangeMap, CachedLoader>>

    /**
     * Provides response for Binder
     */
    private var serviceLastMap: MapDataManifest? = null
    private var serviceLastLoader: CachedLoader? = null

    /**
     * Reference for unsubscribe subscription when service ends
     */
    private lateinit var locationSubscribe: Disposable
    private lateinit var mapChangeSubscribe: Disposable
    private lateinit var mapLoadSubscribe: Disposable

    private val mapChanger = PublishSubject.create<ChangeMap>()

    private var searchSource: Source? = null

    /**
     * Provides request response for searching path between two points and map changing
     */
    inner class LocationServiceBinder : Binder() {

        val lastMap: MapDataManifest?
            get() = serviceLastMap

        val searchSource: Source?
            get() = this@LocationService.searchSource

        fun changeMap(map: ChangeMap) {
            mapChanger.onNext(map)
        }

        fun findPath(from: LevelLocation, to: LevelLocation): List<LevelLocation> {
            val loader = serviceLastLoader ?: return listOf()
            return loader.pathFinder.findPath(
                    LevelPoint(
                            loader.cartesian.convertToPoint(from.coordinates),
                            from.level,
                            null
                    ),
                    LevelPoint(
                            loader.cartesian.convertToPoint(to.coordinates),
                            to.level,
                            null
                    )
            ).map {
                LevelLocation(loader.cartesian.convert(it.point), it.level)
            }
        }

    }

    override fun onCreate() {
        super.onCreate()
        rxBluetooth = RxBleClient.create(this)

        prepareReactiveSources()
        prepareReactiveLocation()

        mapLoadSubscribe = mapRx.subscribe {
            searchSource = Source(it.second.elements, it.second.cartesian)
            serviceLastLoader = it.second
            serviceLastMap = it.first.map
        }

        locationSubscribe = locationRx.subscribe { position ->
            serviceLastLoader?.let { loader ->
                EventBus.getDefault().post(LevelLocation(
                        loader.cartesian.convert(position.point),
                        position.level,
                        position.accuracy,
                        position.compass
                ))
                position.debugging?.let { debug ->
                    EventBus.getDefault().post(BeaconsPositionDebug(
                            debug.beaconsRange.map { beacon ->
                                LevelLocation(
                                        loader.cartesian.convert(beacon.beaconPosition),
                                        position.level,
                                        beacon.distance
                                )
                            }
                    ))
                }
            }

        }

        mapChangeSubscribe = mapRx.subscribe {
            EventBus.getDefault().post(MapHasChanged(it.first.map))
        }

        orientationSensor = ImprovedOrientationSensor1Provider(getSystemService(Context.SENSOR_SERVICE) as SensorManager)
        orientationSensor.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        orientationSensor.stop()
        locationSubscribe.dispose()
        mapChangeSubscribe.dispose()
        mapLoadSubscribe.dispose()
    }

    override fun onUnbind(intent: Intent?): Boolean {
        super.onUnbind(intent)
        return true
    }

    override fun onBind(intent: Intent?): IBinder {
        return LocationServiceBinder()
    }

    /**
     * Prepare reactive loading of service and localisation
     */
    private fun prepareReactiveLocation() {
        mapRx = mapChanger
                .toFlowable(BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.single())
                .distinctUntilChanged()
                .map {
                    it to CachedLoader(applicationContext, it.map.geoJson!!)
                }.share()

        locationRx = mapRx.switchMap { pair ->
            val reactive = ReactiveProcess(
                    walkingRx,
                    compassRx,
                    bleScanningRx,
                    Building(pair.second.elements),
                    pair.second.pathFinder
            )
            reactive.debugging = pair.first.debug
            reactive.positionReactive
        }.share()
    }

    /**
     * Prepare bluetooth service for reactive
     */
    private fun createBluetoothObservableReactiveOne(): Observable<ScanResult> {
        val scanSettings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .build()

        return rxBluetooth
                .observeStateChanges()
                .startWith(rxBluetooth.state)
                .switchMap({
                    when (rxBluetooth.state) {
                        RxBleClient.State.READY -> rxBluetooth
                                .scanBleDevices(scanSettings)
                                .onErrorResumeNext(Observable.empty())
                        else -> Observable.empty()
                    }
                })
    }

    /**
     * Prepares sensor sources in reactive format
     */
    private fun prepareReactiveSources() {
        val walkingDetection = WalkingDetection()

        bleScanningRx = createBluetoothObservableReactiveOne()
                .map { Signal(it.bleDevice.macAddress, it.rssi.toDouble()) }
                .toFlowable(BackpressureStrategy.LATEST)
                .share()

        val accelerometerSharedRx = ReactiveSensors(this)
                .observeSensor(Sensor.TYPE_ACCELEROMETER, SensorManager.SENSOR_DELAY_UI)
                .subscribeOn(Schedulers.computation())
                .filter(ReactiveSensorFilter.filterSensorChanged())
                .share()

        compassRx = Flowable.interval(100, TimeUnit.MILLISECONDS)
                .map {
                    val eulerAngles = FloatArray(3)
                    orientationSensor.getEulerAngles(eulerAngles)
                    convertToCircleAngle(Math.toDegrees(eulerAngles[0].toDouble()))
                }
                .share()

        walkingRx = accelerometerSharedRx
                .map {
                    val event = it.sensorEvent
                    WalkingAccelerometer(
                            event.values[0].toDouble(),
                            event.values[1].toDouble(),
                            event.values[2].toDouble()
                    )
                }
                .buffer(1, TimeUnit.SECONDS)
                .map {
                    walkingDetection.predict(it)
                }
                .share()
    }

    class MapHasChanged(val map: MapDataManifest)

    class BeaconsPositionDebug(val list: List<LevelLocation>)

    class ChangeMap(val map: MapDataManifest, val debug: Boolean = false)

}
