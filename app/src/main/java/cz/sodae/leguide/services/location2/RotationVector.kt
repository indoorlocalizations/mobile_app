package cz.sodae.leguide.services.location2

import android.hardware.SensorEvent
import android.hardware.SensorManager


class RotationVector {
    val NS2S = 1.0f / 1000000000.0f
    val EPSILON = 0.1f
    var deltaRotationVector = FloatArray(4)
    var timestamp = 0L

    /**
     *
     */
    fun change(event: SensorEvent): Double {
        if (timestamp != 0L) {
            val dT = (event.timestamp - timestamp) * NS2S
            // Axis of the rotation sample, not normalized yet.
            var axisX = event.values[0]
            var axisY = event.values[1]
            var axisZ = event.values[2]

            // Calculate the angular speed of the sample
            val omegaMagnitude = Math.sqrt((axisX * axisX + axisY * axisY + axisZ * axisZ).toDouble()).toFloat()

            // Normalize the rotation vector if it's big enough to get the axis
            // (that is, EPSILON should represent your maximum allowable margin of error)
            if (omegaMagnitude > EPSILON) {
                axisX /= omegaMagnitude
                axisY /= omegaMagnitude
                axisZ /= omegaMagnitude
            }

            // Integrate around this axis with the angular speed by the timestep
            // in order to get a delta rotation from this sample over the timestep
            // We will convert this axis-angle representation of the delta rotation
            // into a quaternion before turning it into the rotation matrix.
            val thetaOverTwo = omegaMagnitude * dT / 2.0f
            val sinThetaOverTwo = Math.sin(thetaOverTwo.toDouble()).toFloat()
            val cosThetaOverTwo = Math.cos(thetaOverTwo.toDouble()).toFloat()
            deltaRotationVector[0] = sinThetaOverTwo * axisX
            deltaRotationVector[1] = sinThetaOverTwo * axisY
            deltaRotationVector[2] = sinThetaOverTwo * axisZ
            deltaRotationVector[3] = cosThetaOverTwo
        }
        timestamp = event.timestamp
        val deltaRotationMatrix = FloatArray(9)
        SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector)
        // User code should concatenate the delta rotation we computed with the current rotation
        // in order to get the updated rotation.
        //rotationCurrent = rotationCurrent * deltaRotationMatrix;


        val orientation = FloatArray(3)
        SensorManager.getOrientation(deltaRotationVector, orientation)

        val pitch = orientation[1]
        val roll = orientation[2]

        return orientation[3].toDouble() // yaw / azimuth
    }

}