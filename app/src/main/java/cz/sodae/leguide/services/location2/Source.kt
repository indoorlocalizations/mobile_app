package cz.sodae.leguide.services.location2

import cz.sodae.leguide.frontend.SearchEngine
import cz.sodae.leguide.geoprojection.elements.*
import cz.sodae.leguide.geoprojection.loader.geojson.CartesianPositionConverter
import cz.sodae.leguide.geoprojection.toXYPoint

/**
 * Searching source for [SearchEngine]
 */
class Source(
        elements: List<Element>,
        private val cartesianPositionConverter: CartesianPositionConverter
) : SearchEngine.Source {

    private val map = elements
            .filter { it.identifier != null }
            .filter { it.name?.isNotEmpty() ?: false }
            .map {
                SearchEngine.Searchable(
                        it.name!!,
                        "Floor " + when {
                            it.level.oneLevel() -> it.level.lower
                            else -> "%.1f to %.1f".format(it.level.lower, it.level.upper)
                        },
                        it.identifier!!,
                        SearchEngine.Source.Position(
                                cartesianPositionConverter.convert(it.geometry.centroid.coordinate.toXYPoint()),
                                it.level
                        ),
                        when {
                            it is Door && it.doorType == DoorType.ENTRANCE -> SearchEngine.Specialization.ENTRANCE
                            it is Room && it.toiletAmenity != ToiletAmenity.NONE -> when (it.toiletAmenity) {
                                ToiletAmenity.MALE -> SearchEngine.Specialization.TOILET_MALE
                                ToiletAmenity.FEMALE -> SearchEngine.Specialization.TOILET_FEMALE
                                else -> SearchEngine.Specialization.TOILET
                            }
                            else -> SearchEngine.Specialization.NONE
                        }
                ) to it
            }.toMap()

    override val items: List<SearchEngine.Searchable>
        get() = map.keys.toList()

}
