package cz.sodae.leguide.TestMap

import android.content.Context
import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates
import cz.sodae.leguide.map.MapDataManifest
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

object Vsb {

    @Throws(IOException::class)
    fun readFromAssets(context: Context, filename: String): String {
        val reader = BufferedReader(InputStreamReader(context.assets.open(filename)))

        // do reading, usually loop until end of file reading
        val sb = StringBuilder()
        var mLine: String? = reader.readLine()
        while (mLine != null) {
            sb.append(mLine) // process line
            mLine = reader.readLine()
        }
        reader.close()
        return sb.toString()
    }

    fun createManifest(context: Context): MapDataManifest {
        val centerLat = 49.83139895470
        val centerLong = 18.16108985004

        val mapDataManifest = MapDataManifest(
                "VSB FEI",
                arrayOf(
                        WSG84Coordinates(centerLong - 0.001, centerLat + 0.001, 276.0),
                        WSG84Coordinates(centerLong + 0.001, centerLat - 0.001, 276.0)
                ),
                "17. listopadu, Ostrava"
        )
        mapDataManifest.center = WSG84Coordinates(centerLong, centerLat, 276.0)
        mapDataManifest.allowedZoom = 19
        mapDataManifest.defaultZoom = 19
        mapDataManifest.defaultLevel = 4f

        try {
            mapDataManifest.geoJson = readFromAssets(context, "vsb-indoor.geojson")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return mapDataManifest
    }

}
