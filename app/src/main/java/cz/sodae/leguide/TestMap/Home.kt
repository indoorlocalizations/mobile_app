package cz.sodae.leguide.TestMap

import android.content.Context
import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates
import cz.sodae.leguide.map.MapDataManifest
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

object Home {

    @Throws(IOException::class)
    fun readFromAssets(context: Context, filename: String): String {
        val reader = BufferedReader(InputStreamReader(context.assets.open(filename)))

        // do reading, usually loop until end of file reading
        val sb = StringBuilder()
        var mLine: String? = reader.readLine()
        while (mLine != null) {
            sb.append(mLine) // process line
            mLine = reader.readLine()
        }
        reader.close()
        return sb.toString()
    }

    fun createManifest(context: Context): MapDataManifest {
        val centerLat = 49.790437
        val centerLong = 18.452115

        val mapDataManifest = MapDataManifest(
                "Rodinný dům",
                arrayOf(
                        WSG84Coordinates(centerLong - 0.0006, centerLat + 0.0006, 276.0),
                        WSG84Coordinates(centerLong + 0.0006, centerLat - 0.0006, 276.0)
                ),
                "Havířov"
        )
        mapDataManifest.center = WSG84Coordinates(centerLong, centerLat, 276.0)
        mapDataManifest.allowedZoom = 19
        mapDataManifest.defaultZoom = 19
        mapDataManifest.defaultLevel = 1f

        try {
            mapDataManifest.geoJson = readFromAssets(context, "domov.json")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return mapDataManifest
    }

}
