package cz.sodae.leguide.utils

import java.io.File

object FileUtils {

    /**
     * Delete recursive directory
     *
     * @param dir directory to delete
     */
    fun deleteDirectory(dir: File) {
        if (dir.isDirectory) {
            dir.list()
                    .map { File(dir, it) }
                    .forEach {
                        if (it.isDirectory) {
                            deleteDirectory(it)
                        }
                        it.delete()
                    }
            dir.delete()
        }
    }

}
