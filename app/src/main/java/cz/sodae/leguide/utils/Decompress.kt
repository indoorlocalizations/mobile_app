package cz.sodae.leguide.utils

import android.content.Context
import android.util.Log

import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

import cz.sodae.leguide.geoprojection.Point

object Decompress {
    private val BUFFER_SIZE = 1024 * 10
    private val TAG = "Decompress"

    fun unzipFromAssets(context: Context, zipFile: String, destination: String?) {
        try {
            val stream = context.assets.open(zipFile)
            unzip(stream, destination ?: context.filesDir.absolutePath)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun unzip(zipFile: String, location: String) {
        try {
            val fin = FileInputStream(zipFile)
            unzip(fin, location)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    fun unzip(stream: InputStream, destination: String) {
        dirChecker(destination, "")
        val buffer = ByteArray(BUFFER_SIZE)
        ZipInputStream(stream).use { zin ->
            while (true) {
                val ze = zin.nextEntry ?: break
                Log.v(TAG, "Unzipping " + ze.name)

                if (ze.isDirectory) {
                    dirChecker(destination, ze.name)
                } else {
                    val f = File(destination + ze.name)
                    if (!f.exists()) {
                        FileOutputStream(destination + ze.name).use {
                            while(true) {
                                val count = zin.read(buffer)
                                if (count == -1) break
                                it.write(buffer, 0, count)
                            }
                            zin.closeEntry()
                        }
                    }
                }

            }
        }
    }

    private fun dirChecker(destination: String, dir: String) {
        val f = File(destination + dir)
        if (!f.isDirectory) {
            if (!f.mkdirs()) {
                Log.w(TAG, "Failed to create folder " + f.name)
            }
        }
    }
}