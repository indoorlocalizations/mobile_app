package cz.sodae.leguide.frontend

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import com.mancj.materialsearchbar.MaterialSearchBar
import com.polidea.rxandroidble2.RxBleClient
import cz.sodae.leguide.R
import cz.sodae.leguide.frontend.drawers.webmap.WebMapFragment
import cz.sodae.leguide.services.location2.LevelLocation
import cz.sodae.leguide.services.location2.LocationService
import cz.sodae.leguide.utils.LogTag
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_map.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class MapActivity : AppCompatActivity(), MaterialSearchBar.OnSearchActionListener {

    private val TAG = LogTag("MainActivity")

    private val searchEngine = SearchEngine()
    private val webMapFragment = WebMapFragment()
    private lateinit var searchSuggestionAdapter: SearchSuggestionAdapter
    private var lastLocation: LevelLocation? = null

    private var askerToEnableBluetooth: Disposable? = null

    private var mConnection = object : ServiceConnection {

        private var mBinder: LocationService.LocationServiceBinder? = null

        val connected: Boolean
            get() = mBinder != null

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as LocationService.LocationServiceBinder

            println("Connected")
            if (binder.lastMap == null) {
                finish()
                return
            }

            webMapFragment.onLoadMap(binder.lastMap!!)
            searchEngine.addSource(binder.searchSource!!)
            searchSuggestionAdapter.loadSuggestion()
            mBinder = binder
        }

        override fun onServiceDisconnected(className: ComponentName) {
            Log.i(TAG, "Disconnected")

            searchEngine.clear()
            searchSuggestionAdapter.clearSuggestions()
            mBinder = null
        }

        fun findPath(from: LevelLocation, to: LevelLocation): List<LevelLocation> {
            return mBinder?.findPath(from, to) ?: listOf()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLocationChanged(location: LevelLocation) {
        lastLocation = location
        webMapFragment.onLocationUpdated(location)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onBeaconsDistanceChanged(beacons: LocationService.BeaconsPositionDebug) {
        webMapFragment.onBeaconsDebug(beacons)
    }

    override fun onResume() {
        super.onResume()
        bindService(
                Intent(applicationContext, LocationService::class.java),
                mConnection,
                Context.BIND_AUTO_CREATE
        )
        EventBus.getDefault().register(this)

        val rxBluetooth = RxBleClient.create(this)
        askerToEnableBluetooth = rxBluetooth
                .observeStateChanges()
                .startWith(rxBluetooth.state)
                .subscribe {
                    if (it == RxBleClient.State.BLUETOOTH_NOT_ENABLED) {
                        val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        enableIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        this.startActivity(enableIntent)
                    } else if (it == RxBleClient.State.LOCATION_PERMISSION_NOT_GRANTED) {
                        checkPermission()
                    }
                }
    }

    public override fun onPause() {
        super.onPause()
        if (mConnection.connected) {
            unbindService(mConnection)
        }
        askerToEnableBluetooth?.dispose()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        //setSupportActionBar(toolbar)

        searchSuggestionAdapter = SearchSuggestionAdapter(
                searchEngine,
                getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        )
        searchSuggestionAdapter.clickListener = {
            showSearchedPath(it)
            searchBar.disableSearch()
        }

        fragmentManager
                .beginTransaction()
                .replace(R.id.web_map_fragment, webMapFragment)
                .commit()

        val searchBar = findViewById<MaterialSearchBar>(R.id.searchBar)
        searchBar.setOnSearchActionListener(this)
        searchBar.setCustomSuggestionAdapter(searchSuggestionAdapter)
        searchBar.addTextChangeListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchSuggestionAdapter.filter.filter(searchBar.text)
            }
        })

        if (!checkPermission()) {
            finish()
        }
    }

    override fun onButtonClicked(buttonCode: Int) {
        when (buttonCode) {
            MaterialSearchBar.BUTTON_BACK -> searchBar.disableSearch()
            MaterialSearchBar.BUTTON_NAVIGATION -> finish(); // todo?
        }
    }

    override fun onSearchStateChanged(enabled: Boolean) {

    }

    override fun onSearchConfirmed(text: CharSequence?) {
        searchEngine.search(text.toString()).firstOrNull()?.let {
            showSearchedPath(it)
        }
    }

    private fun showSearchedPath(searchable: SearchEngine.Searchable) {
        val location = lastLocation
        if (location == null) {
            Toast.makeText(this, "Device location is unknown, therefore application cannot find a path.", Toast.LENGTH_SHORT).show()
            searchEngine.onePointLocation(searchable).let {
                showSearchedPoint(searchable)
            }
            return
        }
        val path = searchEngine.getPath(searchable, {
            mConnection.findPath(location, it)
        })
        webMapFragment.showPath(path)
    }

    private fun showSearchedPoint(searchable: SearchEngine.Searchable) {
        val lastLevel = lastLocation?.level ?: 0.0
        val level = when {
            lastLevel in searchable.location.level -> lastLevel
            lastLevel > searchable.location.level.upper -> searchable.location.level.upper
            else -> searchable.location.level.lower
        }
        webMapFragment.showPoint(
                LevelLocation(
                        searchable.location.coordinates,
                        level
                ),
                searchable.title
        )
    }

    private fun checkPermission(): Boolean {
        return if (
                android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            true
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    MainActivity.REQUEST_CODE_PERMISSION_LOCATION
            )
            false
        }
    }

}
