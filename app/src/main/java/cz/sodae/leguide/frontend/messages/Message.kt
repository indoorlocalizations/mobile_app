package cz.sodae.leguide.frontend.messages

/**
 * Message for frontend
 */
interface Message {

    /**
     * @return message distinguisher
     */
    val key: String

    /**
     * @return stringify json
     */
    val data: String

}
