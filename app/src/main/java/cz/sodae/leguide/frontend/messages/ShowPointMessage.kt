package cz.sodae.leguide.frontend.messages

import cz.sodae.leguide.services.location2.LevelLocation
import org.json.JSONException
import org.json.JSONObject

/**
 * Message shows point to user
 */
class ShowPointMessage(private val location: LevelLocation, private val title: String) : Message {

    override val key = "showpoint"

    override val data by lazy {
        try {
            val json = JSONObject()
            json.put("title", title)
            json.put("level", location.level)
            json.put("latitude", location.coordinates.latitude)
            json.put("longitude", location.coordinates.longitude)
            return@lazy json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        "null"
    }

}
