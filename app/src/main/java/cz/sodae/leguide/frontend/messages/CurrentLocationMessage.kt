package cz.sodae.leguide.frontend.messages

import org.json.JSONException
import org.json.JSONObject

import cz.sodae.leguide.services.location2.LevelLocation

/**
 * Message changes user position pointer
 */
class CurrentLocationMessage(private val location: LevelLocation) : Message {

    override val key = "position"

    override val data by lazy {
        try {
            val json = JSONObject()
            json.put("level", location.level)
            json.put("accuracy", location.accuracy)
            json.put("latitude", location.coordinates.latitude)
            json.put("longitude", location.coordinates.longitude)
            json.put("compass", location.compass)
            return@lazy json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        "null"
    }

}
