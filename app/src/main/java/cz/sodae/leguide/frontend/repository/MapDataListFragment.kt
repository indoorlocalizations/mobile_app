package cz.sodae.leguide.frontend.repository

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.sodae.leguide.R
import cz.sodae.leguide.frontend.repository.MapDataListFragment.OnSelectMapListener
import cz.sodae.leguide.map.MapDataManifest
import cz.sodae.leguide.map.MapDatabase

/**
 * A fragment representing a list of maps
 * Activities containing this fragment must implement the [OnSelectMapListener] interface.
 */
class MapDataListFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)
        if (view is RecyclerView) {
            val context = view.getContext()
            view.layoutManager = LinearLayoutManager(context)
            view.adapter = ItemRecyclerViewAdapter(MapDatabase(context).maps) {
                (activity as OnSelectMapListener).onMapSelected(it)
            }
        }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        check(activity is OnSelectMapListener)
    }

    interface OnSelectMapListener {
        /**
         * Callback is called when user pick a map
         *
         * @param item
         */
        fun onMapSelected(item: MapDataManifest)
    }
}
