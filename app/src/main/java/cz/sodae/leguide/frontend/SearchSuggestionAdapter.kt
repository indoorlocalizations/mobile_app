package cz.sodae.leguide.frontend

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter
import cz.sodae.leguide.R

/**
 * Adapter to searching rows
 */
class SearchSuggestionAdapter(val searchEngine: SearchEngine, inflater: LayoutInflater?) :
        SuggestionsAdapter<SearchEngine.Searchable, SearchSuggestionAdapter.SuggestionHolder>(inflater) {

    var clickListener: (SearchEngine.Searchable) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SuggestionHolder {
        val view = layoutInflater.inflate(R.layout.holder_search_element, parent, false)
        return SuggestionHolder(view)
    }

    override fun getSingleViewHeight(): Int = 80

    override fun onBindSuggestionHolder(suggestion: SearchEngine.Searchable, holder: SuggestionHolder, position: Int) {
        holder.title.text = suggestion.title
        holder.description.text = suggestion.description
        holder.itemView.setOnClickListener({
            clickListener(suggestion)
        })
        holder.icon.setImageResource(when (suggestion.specialization) {
            SearchEngine.Specialization.TOILET -> R.drawable.ic_wc_black_24dp
            SearchEngine.Specialization.TOILET_MALE -> R.drawable.ic_wc_black_24dp
            SearchEngine.Specialization.TOILET_FEMALE -> R.drawable.ic_wc_black_24dp
            SearchEngine.Specialization.ENTRANCE -> R.drawable.ic_exit_to_app_black_24dp
            else -> R.drawable.ic_map_black_24dp
        })
    }

    class SuggestionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.holder_search_element_title)
        var description: TextView = itemView.findViewById(R.id.holder_search_element_description)
        var icon: ImageView = itemView.findViewById(R.id.holder_search_element_img)
    }

    fun loadSuggestion() {
        suggestions = searchEngine.recommendations()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val results = FilterResults()
                val term = constraint.toString()
                searchEngine.search(term).let {
                    results.count = it.size
                    results.values = it
                }
                return results
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                suggestions = results.values as List<SearchEngine.Searchable>
                notifyDataSetChanged()
            }
        }
    }

    interface OnClick {
        fun onClick(searchable: SearchEngine.Searchable)
    }

}