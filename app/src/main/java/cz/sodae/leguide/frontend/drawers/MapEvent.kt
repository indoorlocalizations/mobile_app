package cz.sodae.leguide.frontend.drawers

import cz.sodae.leguide.map.MapDataManifest

interface MapEvent {

    fun onLoadMap(mapDataManifest: MapDataManifest)

}
