package cz.sodae.leguide.frontend.repository

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import cz.sodae.leguide.R
import cz.sodae.leguide.map.MapDataManifest

/**
 * Adapter to show maps and produce event on click
 */
class ItemRecyclerViewAdapter(
        private val mValues: List<MapDataManifest>,
        private val mClickListener: (MapDataManifest) -> Unit
) : RecyclerView.Adapter<ItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        holder.mIdView.text = mValues[position].title
        holder.mContentView.text = mValues[position].description

        holder.mView.setOnClickListener {
            mClickListener(mValues[position])
        }
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.findViewById(R.id.id)
        val mContentView: TextView = mView.findViewById(R.id.content)
        var mItem: MapDataManifest? = null

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
