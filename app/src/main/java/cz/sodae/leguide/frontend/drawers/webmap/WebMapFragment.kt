package cz.sodae.leguide.frontend.drawers.webmap

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Fragment
import android.content.pm.ApplicationInfo
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import cz.sodae.leguide.R
import cz.sodae.leguide.frontend.drawers.MapEvent
import cz.sodae.leguide.frontend.drawers.webmap.providers.LocalAsset
import cz.sodae.leguide.frontend.drawers.webmap.providers.Online
import cz.sodae.leguide.frontend.drawers.webmap.providers.Provider
import cz.sodae.leguide.frontend.messages.*
import cz.sodae.leguide.map.MapDataManifest
import cz.sodae.leguide.services.location2.LevelLocation
import cz.sodae.leguide.services.location2.LocationService

@SuppressLint("SetJavaScriptEnabled")
class WebMapFragment : Fragment(), MapEvent {

    private lateinit var webView: WebView

    var mapDataManifest: MapDataManifest? = null

    private var provider: Provider? = null

    override fun onLoadMap(mapDataManifest: MapDataManifest) {
        this.mapDataManifest = mapDataManifest
        this.sendWebAppMessage(LoadMapMessage(mapDataManifest))
    }

    /**
     * Sends location to web app
     */
    fun onLocationUpdated(location: LevelLocation) {
        this.sendWebAppMessage(CurrentLocationMessage(location))
    }

    fun onBeaconsDebug(beacons: LocationService.BeaconsPositionDebug) {
        this.sendWebAppMessage(BeaconsDebug(beacons))
    }

    /**
     * Shows marker in web app
     */
    fun showPoint(point: LevelLocation, title: String) {
        this.sendWebAppMessage(ShowPointMessage(point, title))
    }

    /**
     * Show path in web app
     */
    fun showPath(points: List<LevelLocation>) {
        this.sendWebAppMessage(ShowPathMessage(points))
    }

    /**
     * Sends message to javascript frontend
     *
     * @param message
     */
    private fun sendWebAppMessage(message: Message) {
        webView.loadUrl("javascript:appEvent('" + API_VERSION + "', '" + message.key + "', " + message.data + ")")
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_web_map, container, false)

        provider = LocalAsset(activity, "page.zip")
        provider!!.prepare()
        //provider = Online(activity, "http://10.0.0.20:3333/")

        webView = view.findViewById(R.id.webWindow)
        webView.settings.javaScriptEnabled = true // enable javascript
        webView.webViewClient = WebViewClient()
        webView.addJavascriptInterface(CallableJavascriptInterface(), "LeguideApp")

        if (provider!!.isAvailable) {
            webView.loadUrl(provider!!.url)
        } else {
            webView.loadUrl("file:///android_asset/nonAvailableProvider.html")
        }

        enableDebugging(view.findViewById(R.id.refresh_btn))

        return view
    }

    /**
     * Javascript virtual class
     */
    private inner class CallableJavascriptInterface {
        val mapManifestObject: String
            @JavascriptInterface
            get() = LoadMapMessage(mapDataManifest!!).data

    }

    /**
     * Enable webview debugging only if this app in debug mode
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private fun enableDebugging(refreshButton: View) {
        if (activity.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0) {
            WebView.setWebContentsDebuggingEnabled(true)

            refreshButton.setOnClickListener { webView!!.reload() }
        } else {
            refreshButton.visibility = View.GONE
        }
    }

    companion object {

        private val TAG = "WebMapFragment"

        private val API_VERSION = "0.1"
    }

}

