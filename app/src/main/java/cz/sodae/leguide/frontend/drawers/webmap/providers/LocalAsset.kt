package cz.sodae.leguide.frontend.drawers.webmap.providers

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.util.Log

import java.io.File

import cz.sodae.leguide.utils.Decompress
import cz.sodae.leguide.utils.FileUtils
import cz.sodae.leguide.utils.LogTag

/**
 * Loads frontend from assets
 */
class LocalAsset(private val context: Context, private val filename: String) : Provider {

    override lateinit var url: String

    override val isAvailable: Boolean
        get() = url != null

    override fun prepare() {
        val m = context.packageManager
        var s = context.packageName
        try {
            val p = m.getPackageInfo(s, 0)
            s = p.applicationInfo.dataDir
        } catch (e: PackageManager.NameNotFoundException) {
            Log.w(TAG, "Error Package name not found ", e)
            return
        }

        val destination = "$s/$filename/"
        val file = File(destination)
        file.delete()
        FileUtils.deleteDirectory(file)
        Decompress.unzipFromAssets(context, filename, destination)
        url = "file://$file/index.html"
    }

    companion object {
        private val TAG = LogTag("LocalAsset")
    }


}
