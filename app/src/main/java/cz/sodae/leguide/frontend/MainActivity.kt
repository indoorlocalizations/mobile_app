package cz.sodae.leguide.frontend

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import cz.sodae.leguide.R
import cz.sodae.leguide.frontend.repository.MapDataListFragment
import cz.sodae.leguide.map.MapDataManifest
import cz.sodae.leguide.services.location2.LocationService
import cz.sodae.leguide.utils.LogTag
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Activity to select map
 */
class MainActivity : AppCompatActivity(), MapDataListFragment.OnSelectMapListener {
    companion object {
        const val REQUEST_CODE_PERMISSION_LOCATION = 15643
    }

    private val TAG = LogTag("MainActivity")

    /**
     * Fragment with map list
     */
    private var mapDataListFragment: MapDataListFragment? = null

    /**
     * Enables debug
     */
    private var debug = false

    /**
     * Connection to service
     */
    private var mConnection = object : ServiceConnection {

        private var mBinder: LocationService.LocationServiceBinder? = null

        val connected: Boolean
            get() = mBinder != null

        fun changeMap(map: MapDataManifest): Boolean {
            mBinder?.changeMap(LocationService.ChangeMap(map, debug))

            return mBinder != null
        }

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as LocationService.LocationServiceBinder
            mBinder = binder
        }

        override fun onServiceDisconnected(className: ComponentName) {
            mBinder = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        mapDataListFragment = MapDataListFragment()
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_fragment, mapDataListFragment)
                .commit()

        startService()
        checkPermission()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mConnection.connected) {
            unbindService(mConnection)
        }
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)

        fragmentManager
                .beginTransaction()
                .replace(R.id.main_fragment, mapDataListFragment)
                .commit()
    }

    public override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.enable_debug -> {
                when {
                    debug -> {
                        debug = false
                        item.isChecked = false
                    }
                    else -> {
                        debug = true
                        item.isChecked = true
                    }
                }
                true
            }
            else -> false
        }
    }

    /**
     * Starts service
     */
    private fun startService() {
        bindService(
                Intent(applicationContext, LocationService::class.java),
                mConnection,
                Context.BIND_AUTO_CREATE
        )
    }

    /**
     * Is colled when map is selected by item in list
     */
    override fun onMapSelected(item: MapDataManifest) {
        if (mConnection.changeMap(item)) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_fragment, LoadingFragment())
                    .commit()
        } else {
            Toast.makeText(this, "Failed: service connection", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Map is loaded event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMapLoaded(mapHasChanged: LocationService.MapHasChanged) {
        onMapLoaded()
    }

    /**
     * It starts new activity
     */
    fun onMapLoaded() {
        startActivity(Intent(this, MapActivity::class.java))
    }

    private fun checkPermission(): Boolean {
        return if (
                android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            true
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    REQUEST_CODE_PERMISSION_LOCATION
            )
            false
        }
    }

}
