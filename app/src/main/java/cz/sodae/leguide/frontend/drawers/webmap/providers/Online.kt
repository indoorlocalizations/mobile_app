package cz.sodae.leguide.frontend.drawers.webmap.providers

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * Load frontend from online page
 */
class Online(private val context: Context, override val url: String) : Provider {

    override val isAvailable: Boolean
        get() = isNetworkAvailable

    private val isNetworkAvailable: Boolean
        get() {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

    override fun prepare() {

    }
}
