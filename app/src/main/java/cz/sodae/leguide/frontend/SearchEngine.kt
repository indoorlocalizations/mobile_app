package cz.sodae.leguide.frontend

import android.os.Parcel
import android.os.Parcelable
import cz.sodae.leguide.geoprojection.elements.Levels
import cz.sodae.leguide.geoprojection.utils.GeographicTools
import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates
import cz.sodae.leguide.services.location2.LevelLocation
import java.io.Serializable

/**
 * Class provides search results for search bar
 */
class SearchEngine {

    /**
     * Helper to create specific predefined rows
     */
    private fun createPredefined(key: String, title: String, description: String, specialization: Specialization): Searchable = Searchable(
            title,
            description,
            key,
            Source.Position(WSG84Coordinates(0.0, 0.0), Levels(0.0, 0.0)),
            specialization
    )

    /**
     * Predefined specific rows
     */
    private val predefined: List<Searchable> = listOf(
            createPredefined("_toilet", "Nearest toilet", "It finds nearest toilet for male & female", Specialization.TOILET),
            createPredefined("_toilet_male", "Nearest male toilet", "It finds nearest male toilet", Specialization.TOILET_MALE),
            createPredefined("_toilet_female", "Nearest female toilet", "It finds nearest female toilet", Specialization.TOILET_FEMALE),
            createPredefined("_entrance", "Nearest exit", "I would like go out", Specialization.ENTRANCE)
    )

    /**
     * Source provides rows
     */
    private val sources = mutableListOf<Source>()

    /**
     * Items in searching list
     */
    private var items: List<Searchable> = listOf()

    /**
     * Add source and invalidate database
     */
    fun addSource(source: Source) {
        sources.add(source)
        invalidate()
    }

    /**
     * Remove source and invalidate database
     */
    fun removeSource(source: Source) {
        sources.remove(source)
        invalidate()
    }

    /**
     * Remove all sources and invalidate database
     */
    fun clear() {
        sources.clear()
        items = listOf()
    }

    /**
     * Find path to a searched place
     */
    fun getPath(searchable: Searchable, provider: ((to: LevelLocation) -> List<LevelLocation>)): List<LevelLocation> {
        return predefined.firstOrNull {
            searchable.identifier == it.identifier
        }?.let { type ->
            items
                    .filter { typeIn -> predefined.firstOrNull { typeIn.identifier == it.identifier } == null }
                    .filter {
                        it.specialization == type.specialization
                    }
                    .map {
                        it to provider(LevelLocation(
                                it.location.coordinates,
                                it.location.level.lower
                        ))
                    }
                    .sortedBy {
                        it.second.zipWithNext({ a, b ->
                            GeographicTools.distance(a.coordinates, b.coordinates)
                        }).sum()
                    }.let {
                        it
                    }.firstOrNull()?.second ?: listOf()

        } ?: provider(LevelLocation(
                searchable.location.coordinates,
                searchable.location.level.lower
        ))
    }

    /**
     * Invalidate database of rows
     */
    private fun invalidate() {
        val sourceItems = sources.flatMap { it.items }

        val existingPredefined = predefined.filter { type ->
            sourceItems.filter { it.specialization == type.specialization }.isNotEmpty()
        }

        items = existingPredefined + sourceItems
    }

    /**
     * Returns position if searchable element is one point
     */
    fun onePointLocation(searchable: Searchable): LevelLocation? {
        val special = predefined.filter {
            searchable.identifier == it.identifier
        }.isEmpty()
        return if (!special) {
            LevelLocation(
                    searchable.location.coordinates,
                    searchable.location.level.lower
            )
        } else {
            null
        }
    }

    /**
     * Top items
     */
    fun recommendations(): List<Searchable> {
        return items
    }

    /**
     * Searching by lower case match
     */
    fun search(text: String): List<Searchable> {
        if (text.isEmpty()) {
            return items
        }
        val key = text.toLowerCase()

        return items.filter {
            it.searchableKey.contains(key)
        }
    }

    /**
     * Source provides items for searching
     */
    interface Source {

        /**
         * Items for searching
         */
        val items: List<Searchable>

        data class Position(val coordinates: WSG84Coordinates, val level: Levels)

    }

    /**
     * Specific items
     */
    enum class Specialization {
        TOILET, // no defined male or female
        TOILET_MALE,
        TOILET_FEMALE,
        ENTRANCE,
        NONE
    }

    /**
     * Object of row in database. Serialisation available
     * Parcelable required by Material Search bar
     */
    data class Searchable(
            val title: String,
            val description: String,
            val identifier: String,
            val location: Source.Position,
            val specialization: Specialization
    ) : Serializable, Parcelable {
        val searchableKey = title.toLowerCase()

        companion object CREATOR : Parcelable.Creator<Searchable> {
            override fun createFromParcel(input: Parcel): Searchable {
                return Searchable(
                        input.readString(),
                        input.readString(),
                        input.readString(),
                        Source.Position(
                                WSG84Coordinates(
                                        input.readDouble(),
                                        input.readDouble(),
                                        input.readDouble()
                                ),
                                Levels(
                                        input.readDouble(),
                                        input.readDouble()
                                )
                        ),
                        Specialization.valueOf(input.readString())
                )
            }

            override fun newArray(size: Int) = arrayOfNulls<Searchable>(size)
        }


        override fun writeToParcel(dest: Parcel?, flags: Int) {
            dest?.let {
                it.writeString(title)
                it.writeString(description)
                it.writeString(identifier)
                it.writeDouble(location.coordinates.longitude)
                it.writeDouble(location.coordinates.latitude)
                it.writeDouble(location.coordinates.altitude)
                it.writeDouble(location.level.lower)
                it.writeDouble(location.level.upper)
                it.writeString(specialization.name)
            }
        }

        override fun describeContents() = 0
    }
}

