package cz.sodae.leguide.frontend.messages

import cz.sodae.leguide.services.location2.LocationService
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Message shows beacons distances to user
 */
class BeaconsDebug(private val location: LocationService.BeaconsPositionDebug = LocationService.BeaconsPositionDebug(listOf())) : Message {

    override val key = "beacons"

    override val data by lazy {
        try {
            val json = JSONArray()
            location.list.map {
                val jsonin = JSONObject()
                jsonin.put("level", it.level)
                jsonin.put("radius", it.accuracy)
                jsonin.put("latitude", it.coordinates.latitude)
                jsonin.put("longitude", it.coordinates.longitude)
                jsonin
            }.forEach {
                json.put(it)
            }
            return@lazy json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        "null"
    }

}
