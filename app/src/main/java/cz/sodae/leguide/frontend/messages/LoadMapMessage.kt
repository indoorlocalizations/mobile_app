package cz.sodae.leguide.frontend.messages

import cz.sodae.leguide.map.JsonConverter
import cz.sodae.leguide.map.MapDataManifest

/**
 * Notification about load new map
 */
class LoadMapMessage(private val mapDataManifest: MapDataManifest) : Message {

    private val jsonConverter = JsonConverter()

    override val key: String = "loadmap"

    override val data: String by lazy {
        try {
            jsonConverter.toJson(mapDataManifest)
        } catch (e: JsonConverter.InvalidManifestJsonException) {
            e.printStackTrace()
            "null"
        }
    }

}
