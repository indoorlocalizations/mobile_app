package cz.sodae.leguide.frontend.messages

import cz.sodae.leguide.services.location2.LevelLocation
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Message shows path to user
 */
class ShowPathMessage(private val location: List<LevelLocation>) : Message {

    override val key = "showpath"

    override val data by lazy {
        try {
            val json = JSONObject()
            json.put("path", JSONArray(location.map {
                val point = JSONObject()
                point.put("level", it.level)
                point.put("latitude", it.coordinates.latitude)
                point.put("longitude", it.coordinates.longitude)
                point
            }))
            return@lazy json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        "null"
    }

}
