package cz.sodae.leguide.frontend.drawers.webmap.providers

interface Provider {

    /**
     * Checks pre-requisites, e.g. internet connection, is called before load page
     *
     * @return boolean
     */
    val isAvailable: Boolean

    /**
     * @return return url to load
     */
    val url: String

    /**
     * Initialize, is called on creating web map fragment (or when provider is changed)
     */
    fun prepare()

}
