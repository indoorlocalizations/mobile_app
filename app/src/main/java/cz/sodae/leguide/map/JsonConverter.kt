package cz.sodae.leguide.map

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

/**
 * Converts [MapDataManifest] to/from json
 */
class JsonConverter {

    private val mapper: ObjectMapper = ObjectMapper().apply {
        this.registerModule(KotlinModule())
    }

    @Throws(JsonConverter.InvalidManifestJsonException::class)
    fun toJson(manifest: MapDataManifest): String {
        try {
            return mapper.writeValueAsString(manifest)
        } catch (e: JsonProcessingException) {
            throw InvalidManifestJsonException(e)
        }

    }

    @Throws(JsonConverter.InvalidManifestJsonException::class)
    fun toManifest(json: String): MapDataManifest {
        try {
            return mapper.readValue(json, MapDataManifest::class.java)
        } catch (e: Throwable) {
            throw InvalidManifestJsonException(e)
        }

    }

    inner class InvalidManifestJsonException(throwable: Throwable) : Exception("Invalid data to convert", throwable)

}
