package cz.sodae.leguide.map

import java.io.Serializable

import cz.sodae.leguide.geoprojection.utils.WSG84Coordinates

class MapDataManifest : Serializable {

    /**
     * Name for user like eg. "Ostrava Poruba Campus - VŠB-TUO"
     */
    var title: String = ""
    /**
     * Creates square bound, left-top; right-down
     */
    var boundaries: Array<WSG84Coordinates> = arrayOf()

    /**
     * Description of map for user
     */
    var description = ""

    /**
     * Center of map
     */
    var center: WSG84Coordinates? = null
        get() = if (field != null) {
            field
        } else WSG84Coordinates(
                (this.boundaries[0].latitude + this.boundaries[1].latitude) / 2,
                (this.boundaries[0].longitude + this.boundaries[1].longitude) / 2,
                0.0
        )

    /**
     * Zoom which is default to user view
     * http://wiki.openstreetmap.org/wiki/Zoom_levels
     */
    var defaultZoom = 19
        get() = Math.min(field, allowedZoom)

    /**
     * Limiting zoom-out
     */
    var allowedZoom = 0

    /**
     * Level to be showed to user at first
     */
    var defaultLevel = 0f

    /**
     * Building model in GeoJSON
     */
    var geoJson: String? = null

    constructor(title: String, boundaries: Array<WSG84Coordinates>, description: String = "") {
        this.title = title
        this.boundaries = boundaries
        this.description = description
    }


    companion object {
        const val serialVersionUID = 21139020L
    }


}
