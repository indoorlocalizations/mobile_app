package cz.sodae.leguide.map

import android.content.Context
import cz.sodae.leguide.TestMap.Home
import cz.sodae.leguide.TestMap.Vsb

/**
 * Local database of map
 */
class MapDatabase(private val context: Context) {

    val maps = listOf(
            Home.createManifest(context),
            Vsb.createManifest(context)
    )

}